require('dotenv').config()
const express = require('express');
const cors = require('cors');
const { ApolloServer } = require('apollo-server-express');
const neo4j = require('neo4j-driver');
const { createServer } = require('http');
const { SubscriptionServer } = require('subscriptions-transport-ws');
const { execute, subscribe } = require('graphql');
const { schema } = require('./graphql');
const PORT = 3003;

(async () => {

  const driver = neo4j.driver(
    process.env.DB_URL,
    neo4j.auth.basic(process.env.DB_USER, process.env.DB_PASSWORD)
  );

  const app = express();

  app.use('*', cors());
  app.use('/graphql', express.json());
  
  app.get('/', function(req, res) {
    res.redirect('/graphql');
  });

  const server = new ApolloServer({ 
    schema, 
    context: { driver },
    subscriptions: {
      onConnect: async (connectionParams, webSocket) => {
        console.log('connectionParams', connectionParams);
      },
    }
  });

  await server.start();

  const httpServer = createServer(app);

  const subscriptionServer = SubscriptionServer.create({
    schema,
    execute,
    subscribe,
  }, {
    server: httpServer,
    path: '/subscriptions'
  });

  ['SIGINT', 'SIGTERM'].forEach(signal => {
    process.on(signal, () => subscriptionServer.close());
  });
  server.applyMiddleware({ app });

  httpServer.listen(PORT, '0.0.0.0', () => {
    console.log(
      `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`,
    );
  });
})();