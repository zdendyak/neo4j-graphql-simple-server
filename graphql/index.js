const { makeAugmentedSchema } = require('neo4j-graphql-js');

const { typeDefs } = require('./typeDefs');
const { resolvers } = require('./resolvers');

module.exports.schema = makeAugmentedSchema({ typeDefs, resolvers });
