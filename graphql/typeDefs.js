const typeDefs = `
scalar JSON
scalar JSONObject

type Webform {
  key: String!

  sessions: [Context] @relation(name:"SESSION" direction:IN)
  sections: [Section] @relation(name:"PART_OF" direction:IN)
}

type Context {
  sessionId: String!
  channel: String!
  data: String

  form: [Webform] @relation(name:"SESSION" direction:OUT)
}

type Section {
  id: String!

  form: [Webform] @relation(name:"PART_OF" direction:OUT)
}

type LiveMergeField {
  name: String!
  data: String
}

type LiveMergeFieldSub {
  type: String!
  mergeField: LiveMergeField
}

type Subscription {
  sectionAdded(key: String!): Section
  sessionAdded(key: String!): Context
  mergeFieldChanged(name: String!): LiveMergeFieldSub
}
`;

module.exports = {
  typeDefs
};
