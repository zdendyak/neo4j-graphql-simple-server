const { neo4jgraphql, cypherQuery, cypherMutation } = require('neo4j-graphql-js');
const { GraphQLJSON, GraphQLJSONObject } = require('graphql-type-json');
const { pubsub, withFilter } = require('./pubsub');

const MERGE_FIELD_UPDATED = 'MERGE_FIELD_UPDATED';
const MERGE_FIELD_CREATED = 'MERGE_FIELD_CREATED';
const MERGE_FIELD_REMOVED = 'MERGE_FIELD_REMOVED';


module.exports.resolvers = {
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,
  // Query: {
  //   Webform (object, params, ctx, resolveInfo) {
  //     Promise.resolve(cypherQuery(params, ctx, resolveInfo)).then(res => console.log('webform', res))
  //     return neo4jgraphql(object, params, ctx, resolveInfo);
  //   },
  //   Section (object, params, ctx, resolveInfo) {
  //     Promise.resolve(cypherQuery(params, ctx, resolveInfo)).then(res => console.log('section', res))
  //     return neo4jgraphql(object, params, ctx, resolveInfo);
  //   },
  //   Context (object, params, ctx, resolveInfo) {
  //     Promise.resolve(cypherQuery(params, ctx, resolveInfo)).then(res => console.log('context', res))
  //     return neo4jgraphql(object, params, ctx, resolveInfo);
  //   }
  // },
  Mutation: {
    async CreateSection (object, params, ctx, resolveInfo) {
      // Promise.resolve(cypherMutation(params, ctx, resolveInfo)).then(res => console.log('create section', { res, object, params }));
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish('sectionAdded', { sectionAdded: result });
      return result;
    },
    async CreateContext (object, params, ctx, resolveInfo) {
      // Promise.resolve(cypherMutation(params, ctx, resolveInfo)).then(res => console.log('create section', { res, object, params, resolveInfo }));
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish('sessionAdded', { sessionAdded: result });
      return result;
    },
    async CreateLiveMergeField (object, params, ctx, resolveInfo) {
      // Promise.resolve(cypherMutation(params, ctx, resolveInfo)).then(res => console.log('create merge field', { res, object, params }));
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish(MERGE_FIELD_CREATED, { mergeField: result, action: 'create' });
      return result;
    },
    async MergeLiveMergeField (object, params, ctx, resolveInfo) {
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish(MERGE_FIELD_UPDATED, { mergeField: result, action: 'merge' });
      return result;
    },
    async UpdateLiveMergeField (object, params, ctx, resolveInfo) {
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish(MERGE_FIELD_UPDATED, { mergeField: result, action: 'update' });
      return result;
    },
    async DeleteLiveMergeField (object, params, ctx, resolveInfo) {
      const result = await neo4jgraphql(object, params, ctx, resolveInfo);
      pubsub.publish(MERGE_FIELD_REMOVED, { mergeField: result, action: 'delete' });
      return result;
    },
  },
  Subscription: {
    sectionAdded: {
      subscribe: () => pubsub.asyncIterator('sectionAdded')
    },
    sessionAdded: {
      subscribe: () => pubsub.asyncIterator('sessionAdded')
    },
    mergeFieldChanged: {
      subscribe: withFilter(
        () => pubsub.asyncIterator([MERGE_FIELD_CREATED, MERGE_FIELD_UPDATED, MERGE_FIELD_REMOVED]),
        (payload, variables) => {
          return (payload.mergeField.name === variables.name);
        },
      ),
      resolve: (payload, args, context, info) => {
        return {
          type: payload.action,
          mergeField: payload.mergeField
        };
      },
    }
  },
};